import PySimpleGUI as sg
import asyncio
import datetime
import registration

sg.theme('Dark Purple 5')
users_messages : dict = {}

layout = [
    [sg.Text('Message'), sg.InputText(key='MESSAGE'), sg.Checkbox('Show my name')],
    [sg.Text(size=(80, 20), key='CHAT')],
    [sg.Submit(), sg.Cancel()]
    ]

window = sg.Window('Chatroom', layout)

username = registration.register()

def show_window():

    while True:

        global users_messages
        event, values = window.read()

        if event in('Submit'):
            message = values['MESSAGE']
            users_messages[username] = message
            window['CHAT'].update(values['MESSAGE'])

        if event in(None, 'Exit', 'Cancel'):
            window.close()

show_window()
print(users_messages)

#if __name__=='__main__':
 #   loop = asyncio.get_event_loop()
  #  loop.run_until_complete(show_window())
